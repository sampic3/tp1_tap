package com.dinfogarneau.coursmobile.travail1.taptap;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {
    private Button bt_resetHighScore;
    private Button bt_ok;
    private Boolean reset = false;
    private EditText et_pseudo;
    private ImageView iv_share;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        et_pseudo = findViewById(R.id.et_pseudo);
        bt_resetHighScore = findViewById(R.id.bt_resetHighScore);
        bt_resetHighScore.setOnClickListener(this);
        bt_ok = findViewById(R.id.bt_ok);
        bt_ok.setOnClickListener(this);
        iv_share = findViewById(R.id.iv_share);
        iv_share.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.bt_resetHighScore:
                Log.d("tag","Click: reset");
                Intent intenths = new Intent();
                intenths.putExtra("resetH",true);
                setResult(RESULT_OK,intenths);
                finish();
                break;
            case R.id.bt_ok:
                Log.d("tag","Click: OK");
                Intent intent_pseudo = new Intent();
                intent_pseudo.putExtra("pseudo",et_pseudo.getText().toString());
                setResult(RESULT_OK,intent_pseudo);
                finish();
                break;
            case R.id.iv_share:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT,"Mon score: " + getIntent().getStringExtra("highscore"));
                intent.setType("text/plain");
                startActivity(intent);
                break;

        }

    }


}