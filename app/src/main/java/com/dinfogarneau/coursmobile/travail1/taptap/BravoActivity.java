package com.dinfogarneau.coursmobile.travail1.taptap;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class BravoActivity extends AppCompatActivity {
    private TextView tv_scoreBravo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bravo);
        Intent receiveIntent = getIntent();
        tv_scoreBravo = findViewById(R.id.tv_scoreBravo);
        tv_scoreBravo.setText(receiveIntent.getStringExtra("score") + " ms!");
    }
}
