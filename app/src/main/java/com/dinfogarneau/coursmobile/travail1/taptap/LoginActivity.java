package com.dinfogarneau.coursmobile.travail1.taptap;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText et_username;
    private EditText et_password;
    private Button bt_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
        bt_login = findViewById(R.id.bt_login);
        bt_login.setOnClickListener(this);
        Log.d("TAG", "onCreate Main Activity");



    }


    private boolean Authentification(){
        if(et_username.getText().toString().equals("bot") && et_password.getText().toString().equals("123")){
            Log.i("TAG", "login success");
            return true;
        }
        Log.i("TAG", "login failed");
        Toast.makeText(this, getString(R.string.erreurAuthentification), Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.bt_login){
            Log.d("TAG", "login click");

            if (Authentification()) {
                startActivity(new Intent(this,JeuActivity.class));
            }
        }
    }


}