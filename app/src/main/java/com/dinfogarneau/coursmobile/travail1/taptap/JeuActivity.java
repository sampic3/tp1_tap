package com.dinfogarneau.coursmobile.travail1.taptap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class JeuActivity extends AppCompatActivity implements View.OnClickListener {
    private Button bt_settings;
    private Button bt_go;
    private Button bt_1;
    private Button bt_2;
    private Button bt_3;
    private Button bt_4;
    private TextView tv_score;
    private TextView tv_playerName;
    private boolean zero= false;
    private boolean one= false;
    private boolean two= false;
    private boolean three= false;
    private long[] playerTime = new long[4];
    private long highScore=0;
    private static final float ALPHA_BUTTON = 0.6f;
    private static final int REQ_SETTINGS = 111;
    private String pseudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt_settings = findViewById(R.id.bt_settings);
        bt_settings.setOnClickListener(this);
        bt_go = findViewById(R.id.bt_go);
        bt_go.setOnClickListener(this);
        bt_1 = findViewById(R.id.bt_1);
        bt_1.setOnClickListener(this);
        bt_2 = findViewById(R.id.bt_2);
        bt_2.setOnClickListener(this);
        bt_3 = findViewById(R.id.bt_3);
        bt_3.setOnClickListener(this);
        bt_4 = findViewById(R.id.bt_4);
        bt_4.setOnClickListener(this);
        tv_score = findViewById(R.id.tv_score);
        tv_score.setText("0");
        tv_playerName = findViewById(R.id.tv_playerName);
        load_preference();
    }

    private void load_preference(){
        SharedPreferences preference = getSharedPreferences("taptap", MODE_PRIVATE);
        pseudo = preference.getString("pseudo","bot");
        highScore = preference.getLong("highscore",0);
        mettreAJourLePseudo();
        mettreAJourLeHighScore();
    }

    private void mettreAJourLePseudo(){
        tv_playerName.setText("player name: " + pseudo);
    }

    private void mettreAJourLeHighScore(){
        tv_score.setText(Long.toString(highScore));
    }

    @Override
    protected void onActivityResult(int req, int res, @Nullable Intent data) {
        super.onActivityResult(req, res, data);

        if(req == REQ_SETTINGS && res == RESULT_OK)
        {
            pseudo = data.getStringExtra("pseudo");
            if("".equals(pseudo) || pseudo == null)
            {
                pseudo = "bot";
            }

            mettreAJourLePseudo();

            Boolean resetH = data.getBooleanExtra("resetH",false);
            Log.d("TAG", "reset" + Boolean.toString(resetH) );

            if(resetH)
            {
                highScore = 0;
                mettreAJourLeHighScore();
            }
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences preference = getSharedPreferences("taptap", MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();
        editor.putString("pseudo",pseudo);
        editor.putLong("highscore",highScore);
        editor.apply();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_settings:
                Intent settings_intent = new Intent(this,SettingsActivity.class);
                settings_intent.putExtra("highscore", Long.toString(highScore));
                startActivityForResult(settings_intent,REQ_SETTINGS);
                break;
            case R.id.bt_go:
                resetButton();
                break;
            case R.id.bt_1:
                if(!zero){
                    zero = true;
                    one = true;
                    playerTime[0] = SystemClock.elapsedRealtime();
                    bt_1.setText("Go!");
                    bt_1.setAlpha(ALPHA_BUTTON);
                }
                break;
            case R.id.bt_2:
                if(one){
                    one = false;
                    playerTime[1] = SystemClock.elapsedRealtime();
                    bt_2.setText((int) (playerTime[1]-playerTime[0]) +" ms");
                    bt_2.setAlpha(ALPHA_BUTTON);
                    two = true;
                }
                break;
            case R.id.bt_3:
                if(two){
                    two = false;
                    playerTime[2] = SystemClock.elapsedRealtime();
                    bt_3.setText((int) (playerTime[2]-playerTime[0]) +" ms");
                    bt_3.setAlpha(ALPHA_BUTTON);
                    three = true;
                }
                break;
            case R.id.bt_4:
                if(three){
                    three = false;
                    playerTime[3] = SystemClock.elapsedRealtime();
                    bt_4.setText((int) (playerTime[3]-playerTime[0]) +" ms");
                    bt_4.setAlpha(ALPHA_BUTTON);

                    VerifierVictoire();
                    ModifierHighScore();
                }

                break;
        }



    }

    private void VerifierVictoire(){
        // start une nouvelle activité si le joueur à cliquer les 4 bouttons entre 1000 et 2000ms.
        if(playerTime[3]-playerTime[0] > 1000 && playerTime[3]-playerTime[0] < 2000){
            resetButton();
            Intent intentBravo = new Intent(this,BravoActivity.class);
            intentBravo.putExtra("score",Long.toString(playerTime[3]-playerTime[0]));
            startActivity(intentBravo);
        }
    }

    private void ModifierHighScore(){
        // Ajuste le highscore seulement si le score obtenu est inférieur au précédent.
        if(highScore == 0 || playerTime[3]-playerTime[0] < highScore){
            highScore = playerTime[3] - playerTime[0];
            mettreAJourLeHighScore();
        }

    }


    private void resetButton(){
        zero = false;
        bt_1.setText("1");
        bt_1.setAlpha(1);
        bt_2.setText("2");
        bt_2.setAlpha(1);
        bt_3.setText("3");
        bt_3.setAlpha(1);
        bt_4.setText("4");
        bt_4.setAlpha(1);
    }






}